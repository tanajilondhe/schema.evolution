package com.tml.schema.evolution

import com.tml.schema.evolution.mapper.Mapper

class XmlMapper implements Mapper {

	@Override
	public def serialize(def schema) {
		
		//write ur logic here
		schema.TrdCaptRpt.RptSide.each{ rptSide ->
			//access 'RptSide' attribute
			println("rptSide.@OrdID : "+rptSide.@OrdID);
			println("rptSide.@PosEfct : "+rptSide.@PosEfct);
			
			rptSide.Pty.each{pty ->
				println(pty.@ID + " : " + pty.@R);
				if(pty.@R.equalsIgnoreCase("24")){
					pty.@ID = "TMLBIN"
				}
			}
		}
		
		return schema;
	}

	@Override
	public String getStartingElementName() {
		return "TrdCaptRpt";
	}

}
