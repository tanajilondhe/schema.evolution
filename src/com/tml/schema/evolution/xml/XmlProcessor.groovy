package com.tml.schema.evolution.xml



import com.tml.schema.evolution.core.engine.Processor

class XmlProcessor implements Processor {

	@Override
	public def read(String filePath) {
		def file = new File(filePath);
		def xml = new XmlParser().parseText(file.getText());
		return xml;
	}

	@Override
	public void write(def schema, String filePath) {
		def xmlString = removeXmlDeclearation(schema);
		def xmlData = parseSpecialCharacter(xmlString);
		new File(filePath).withWriter('UTF-8') { writer -> writer.writeLine xmlData }
	}


	private def removeXmlDeclearation(def xml) {

		def xmlString = XmlUtils.getXmlAsString(xml);

		int startIndex = xmlString.indexOf("<?xml ");
		int endIndex = xmlString.indexOf("?>");
		if ( startIndex != -1 && endIndex != -1 && endIndex>startIndex){
			xmlString=xmlString.substring(endIndex+2, xmlString.length());
		}

		return xmlString;
	}

	private def parseSpecialCharacter(def xml) {
		return xml.replaceAll(/&lt;/, '<')
				.replaceAll(/&gt;/, '>')
				.replaceAll(/&quot;/, '"')
				.replaceAll(/&apos;/, "'")
				.replaceAll(/&amp;/, '&');
	}

	private def convertSpecialCharacter(def xml) {
		return xml.replaceAll('<', /&lt;/)
				.replaceAll('>', /&gt;/)
				.replaceAll('"', /&quot;/)
				.replaceAll("'", /&apos;/)
				.replaceAll('&', /&amp;/);
	}
}
