package com.tml.schema.evolution.xml

import groovy.xml.XmlUtil

class XmlUtils {

	public static def getXmlAsString(def xml) {
		def xmlOutput = new StringWriter()
		XmlUtil.serialize (xml, xmlOutput);
		return  xmlOutput.toString();
	}
}
