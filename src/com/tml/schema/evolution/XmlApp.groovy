package com.tml.schema.evolution

import com.tml.schema.evolution.core.engine.Engine
import com.tml.schema.evolution.core.engine.ExtensionType
import com.tml.schema.evolution.core.engine.PropertiesCache
import com.tml.schema.evolution.core.engine.SchemaEngine
import com.tml.schema.evolution.mapper.Mapper


String suitePath = "C:/Users/Tanaji/Desktop/xml/";

Mapper mapper = new XmlMapper();

Engine engine = new SchemaEngine(suitePath ,mapper, ExtensionType.XML);
engine.start();

println engine.getModifiedFiles()