package com.tml.schema.evolution


import com.tml.schema.evolution.core.engine.Engine
import com.tml.schema.evolution.core.engine.ExtensionType
import com.tml.schema.evolution.core.engine.SchemaEngine
import com.tml.schema.evolution.mapper.Mapper



def suitePath = "C:/Users/Tanaji/Desktop/jsons/json"

Mapper mapper = new JsonMapper();

Engine engine = new SchemaEngine(suitePath, mapper, ExtensionType.JSON);
engine.start();

println engine.getModifiedFiles()