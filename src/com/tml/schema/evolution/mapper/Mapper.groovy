package com.tml.schema.evolution.mapper

interface Mapper {

	def serialize(def schema);
	
	String getStartingElementName();
	
}
