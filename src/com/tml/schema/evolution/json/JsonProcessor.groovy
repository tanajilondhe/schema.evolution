package com.tml.schema.evolution.json




import groovy.json.JsonBuilder
import groovy.json.JsonSlurper

import com.tml.schema.evolution.core.engine.Processor

class JsonProcessor implements Processor {

	@Override
	public def read(String filePath) {
		def file = new File(filePath);
		def json = new JsonSlurper().parse(file);
		return json;
	}

	@Override
	public void write(def schema, String filePath) {
		new File(filePath).write(new JsonBuilder(schema).toPrettyString());
	}
}
