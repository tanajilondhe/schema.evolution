package com.tml.schema.evolution.core.engine

interface Engine {

	void start();
	
	def getModifiedFiles();
}
