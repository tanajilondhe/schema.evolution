package com.tml.schema.evolution.core.engine



class PropertiesCache {

	private Properties configProperties = new Properties();

	public PropertiesCache() {
		InputStream stream = this.getClass().getClassLoader().getResourceAsStream("config.properties");
		configProperties.load(stream);
	}

	public String getProperty(String key){
		return configProperties.getProperty(key);
	}

	public Set<String> getPropertyNames(){
		return configProperties.stringPropertyNames();
	}

	public boolean containsKey(String key){
		return configProperties.containsKey(key);
	}
}
