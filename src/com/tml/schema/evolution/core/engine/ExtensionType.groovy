package com.tml.schema.evolution.core.engine

import com.tml.schema.evolution.json.JsonProcessor
import com.tml.schema.evolution.xml.XmlProcessor

enum ExtensionType {

	XML (XmlProcessor),
	JSON (JsonProcessor)

	private Class processorClass;

	private ExtensionType(Class processor) {
		this.processorClass = processor;
	}

	public Class getProcessorClass() {
		return processorClass;
	}
}
