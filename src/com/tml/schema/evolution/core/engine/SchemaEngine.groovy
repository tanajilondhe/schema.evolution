package com.tml.schema.evolution.core.engine

import com.tml.schema.evolution.file.searching.FileEngine
import com.tml.schema.evolution.mapper.Mapper
import com.tml.schema.evolution.xml.XmlProcessor

class SchemaEngine implements Engine {

	private String suitePath;
	private Mapper mapper;
	private ExtensionType extensionType

	private Processor processor;
	private def modifiedFiles = [];

	public SchemaEngine(Mapper mapper, ExtensionType extensionType) {
		this.suitePath = new PropertiesCache().getProperty("suitePath");
		this.mapper = mapper;
		this.extensionType = extensionType;
		this.processor = extensionType.getProcessorClass().newInstance();
	}

	public SchemaEngine(String suitePath, Mapper mapper, ExtensionType extensionType) {
		this.suitePath = suitePath;
		this.mapper = mapper;
		this.extensionType = extensionType;
		this.processor = extensionType.getProcessorClass().newInstance();
	}

	@Override
	public void start() {

		if(suitePath == null || suitePath.isEmpty()) {
			throw new IllegalArgumentException("Please specify suitePath.")
		}

		FileEngine engine = new FileEngine();

		def files = engine.getAllFiles(suitePath, extensionType.toString().toLowerCase());

		println("Total files found  : "+files.size());

		files.each{filePath ->

			def schema = processor.read(filePath);

			try{
				String startingElement = mapper.getStartingElementName();

				if(!isNull(schema, startingElement)) {
					schema = mapper.serialize(schema);
					processor.write(schema, filePath);
					modifiedFiles << filePath;
				}else{
					println "schema is not proper with extension("+extensionType+") starting tag("+startingElement+") for path("+filePath+")";
				}
			}catch(MissingPropertyException missngException ) {
				missngException.printStackTrace();
			}
		}
	}

	@Override
	public Object getModifiedFiles() {
		return modifiedFiles;
	}

	private void validExtension() {
		boolean isExtensionCorrect = false;
		ExtensionType.values().each { extension ->
			if(extensionType == extension) {
				if(extensionType.getProcessorClass() == extension.getProcessorClass()){
					isExtensionCorrect = true;
				}
			}
		}

		if(!isExtensionCorrect) {
			throw new IllegalArgumentException("Please specify correct file fornat");
		}
	}

	private boolean isNull(def schema, def schemaElement) {
		def element = schema."${schemaElement}"
		if(isCollectionOrArray(element)){
			return (element.size() == 0);
		}else {
			return element == null;
		}
		return true;
	}

	private boolean isCollectionOrArray(def schemaElement) {
		[Collection, Object[]].any { it.isAssignableFrom(schemaElement.getClass()) }
	}
}

