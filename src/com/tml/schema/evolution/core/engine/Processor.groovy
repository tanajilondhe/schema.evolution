package com.tml.schema.evolution.core.engine

interface Processor {

	public def read(String filePath);
	
	public void write(def schema, String filePath)
}
