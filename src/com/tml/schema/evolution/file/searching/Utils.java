package com.tml.schema.evolution.file.searching;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

	public static final String JSON = "json";
	public static final String XML = "xml";

	public static boolean isExactWordContains(String content, String word) {
		String pattern = "\\b" + word + "\\b";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(content);
		return m.find();
	}
}
