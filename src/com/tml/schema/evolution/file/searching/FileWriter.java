package com.tml.schema.evolution.file.searching;

import java.io.BufferedWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Callable;

public class FileWriter implements Callable<String> {

	private String filePath;
	private String oldWord;
	private String newWord;

	public FileWriter(String filePath, String oldWord, String newWord) {
		this.filePath = filePath;
		this.oldWord = oldWord;
		this.newWord = newWord;
	}

	public String call() throws Exception {

		Path path = Paths.get(filePath);
		byte[] readBytes = Files.readAllBytes(path);
		String content = new String(readBytes, "UTF-8");

		if (Utils.isExactWordContains(content, oldWord)) {
			Charset charset = Charset.forName("US-ASCII");
			BufferedWriter bufferedWriter = Files.newBufferedWriter(path,
					charset);
			String newContent = content.replaceAll(oldWord, newWord);
			bufferedWriter.write(newContent, 0, newContent.length());
			bufferedWriter.flush();
			bufferedWriter.close();
			return filePath;
		}

		return null;
	}

}