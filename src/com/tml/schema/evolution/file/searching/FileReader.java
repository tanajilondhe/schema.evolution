package com.tml.schema.evolution.file.searching;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Callable;

public class FileReader implements Callable<String> {

	private String filePath;
	private String word;

	public FileReader(String filePath, String word) {
		this.filePath = filePath;
		this.word = word;
	}

	public String call() throws Exception {
		Path path = Paths.get(filePath);
		byte[] readBytes = Files.readAllBytes(path);
		String content = new String(readBytes, "UTF-8");
		if (Utils.isExactWordContains(content, word)) {
			return filePath;
		} else {
			return null;
		}
	}

}