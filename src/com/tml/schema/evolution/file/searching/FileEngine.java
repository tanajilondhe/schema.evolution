package com.tml.schema.evolution.file.searching;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

public class FileEngine {

	public List<String> getAllFiles(String filePath, String extension) {

		ForkJoinPool pool = new ForkJoinPool();
		FolderProcessor processor = new FolderProcessor(filePath, extension);
		pool.execute(processor);
		pool.shutdown();
		List<String> results = processor.join();

		return results;
	}

	public List<String> getFilesContains(String filePath, String extension,
			String word) throws InterruptedException, ExecutionException {

		List<String> files = getAllFiles(filePath, extension);

		ExecutorService executorService = Executors.newFixedThreadPool(Runtime
				.getRuntime().availableProcessors());

		Set<Future<String>> futures = new HashSet<Future<String>>();

		for (String path : files) {
			Future<String> future = executorService.submit(new FileReader(
					path, word));
			futures.add(future);
		}

		List<String> filesFound = new ArrayList<String>();
		for (Future<String> future : futures) {
			if (future.get() != null)
				filesFound.add(future.get());
		}

		executorService.shutdown();

		return filesFound;
	}

	public List<String> replace(String filePath, String extension,
			String oldWord, String newWord) throws InterruptedException,
			ExecutionException {

		List<String> files = getAllFiles(filePath, extension);

		ExecutorService executorService = Executors.newFixedThreadPool(Runtime
				.getRuntime().availableProcessors());

		Set<Future<String>> futures = new HashSet<Future<String>>();

		for (String path : files) {
			Future<String> future = executorService.submit(new FileWriter(
					path, oldWord, newWord));
			futures.add(future);
		}

		// futures.stream().filter(future ->future.get()!=
		// null).map(future->future.get()).collect(Collectors.toList());
		List<String> filesFound = new ArrayList<String>();
		for (Future<String> future : futures) {
			if (future.get() != null)
				filesFound.add(future.get());
		}

		executorService.shutdown();

		return filesFound;
	}
}